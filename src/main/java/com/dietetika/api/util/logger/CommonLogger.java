package com.dietetika.api.util.logger;

import com.dietetika.api.exception.FileAppenderException;
import org.apache.log4j.Logger;

public class CommonLogger {
    private static final String KEY_LOGGER = "[DK-COMMON]";

    private CommonLogger() { }

    public static Logger getLogger() {
        return new AbstractLogger().getLogger( KEY_LOGGER );
    }

    public static void initLogger(String ruta) throws FileAppenderException {
        new AbstractLogger().createFileLogger( KEY_LOGGER, ruta );
    }
}
