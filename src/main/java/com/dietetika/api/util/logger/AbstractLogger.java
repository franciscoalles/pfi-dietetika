package com.dietetika.api.util.logger;

import java.io.IOException;

import com.dietetika.api.exception.FileAppenderException;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Layout;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;

public class AbstractLogger {

    private LoggerContainer container = LoggerContainer.getInstance();
    private static final String DEFAULT_PATTERN = "%d{yyyy-MM-dd HH:mm:ss} %-5p %c{1}:%L - %m%n";

    public Logger getLogger(String name) {
        if (name == null) {
            return null;
        } else {
            if (!this.container.contains(name)) {
                this.createConsoleLogger(name);
            }

            return this.container.get(name);
        }
    }

    public void createConsoleLogger(String name) {
        if (name != null) {
            Logger logger = Logger.getLogger(name);
            logger.removeAllAppenders();
            Layout layout = new PatternLayout("%d{yyyy-MM-dd HH:mm:ss} %-5p %c{1}:%L - %m%n");
            ConsoleAppender appender = new ConsoleAppender(layout);
            logger.addAppender(appender);
            this.addLogger(logger);
        }
    }

    public void createFileLogger(String name, String filename) throws FileAppenderException {
        if (name != null && filename != null) {
            this.createFileLogger(name, filename, "%d{yyyy-MM-dd HH:mm:ss} %-5p %c{1}:%L - %m%n");
        }
    }

    public void createFileLogger(String name, String filename, String pattern) throws FileAppenderException {
        if (name != null && filename != null && pattern != null) {
            Logger logger = Logger.getLogger(name);
            logger.removeAllAppenders();
            PatternLayout layout = new PatternLayout(pattern);

            RollingFileAppender appender;
            try {
                appender = new RollingFileAppender(layout, filename);
                appender.setMaxFileSize("5MB");
                appender.setMaxBackupIndex(10);
            } catch (IOException var8) {
                throw new FileAppenderException(var8, filename);
            }

            logger.addAppender(appender);
            this.addLogger(logger);
        }
    }

    private void addLogger(Logger logger) {
        if (logger != null) {
            this.container.put(logger.getName(), logger);
        }

    }
}
