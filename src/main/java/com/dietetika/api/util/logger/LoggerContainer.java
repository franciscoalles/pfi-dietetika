package com.dietetika.api.util.logger;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class LoggerContainer {
    private static LoggerContainer instance;
    private Map<String, Logger> loggerMap = new HashMap();

    public void put(String name, Logger logger) {
        if (name != null && logger != null) {
            this.loggerMap.put(name, logger);
        }

    }

    public Logger get(String name) {
        return name != null && this.loggerMap.containsKey(name) ? (Logger)this.loggerMap.get(name) : null;
    }

    public static LoggerContainer getInstance() {
        if (instance == null) {
            createInstance();
        }

        return instance;
    }

    private static void createInstance() {
        Class var0 = LoggerContainer.class;
        synchronized(LoggerContainer.class) {
            if (instance == null) {
                instance = new LoggerContainer();
            }

        }
    }

    private LoggerContainer() {
    }

    public boolean contains(String name) {
        return name == null ? false : this.loggerMap.containsKey(name);
    }
}
