package com.dietetika.api.service;

import com.dietetika.api.model.calendar.CalendarEvent;
import com.dietetika.api.model.check.Note;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.model.user.Professional;
import com.dietetika.api.model.user.User;
import com.dietetika.api.repository.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class NoteService {

    @Autowired
    private NoteRepository repository;

    public Note create(Professional professional, Patient patient, Note note) {
        note.setPatient( patient );
        note.setProfessional( professional );
        note.setUpdated( note.getCreated() );

        this.repository.save(note);

        return note;
    }

    public List<Note> getPatientNotes(Professional professional, Patient patient) {
        return patient.getNotes().stream()
                .filter(note -> note.getProfessional().is(professional) )
                .sorted(Comparator.comparing(Note::getCreated))
                .collect(Collectors.toList());
    }
}