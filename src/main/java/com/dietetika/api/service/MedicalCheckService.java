package com.dietetika.api.service;

import com.dietetika.api.model.check.MedicalCheck;
import com.dietetika.api.model.check.Note;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.model.user.Professional;
import com.dietetika.api.repository.MedicalCheckRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MedicalCheckService {

    @Autowired
    private MedicalCheckRepository repository;

    public MedicalCheck create(Professional professional, Patient patient, MedicalCheck check) {
        check.setPatient( patient );
        check.setProfessional( professional );
        check.setUpdated( check.getCreated() );

        this.repository.save(check);

        return check;
    }

    public List<MedicalCheck> getPatientChecks(Professional professional, Patient patient) {
        return patient.getMedicalChecks().stream()
                .filter(note -> note.getProfessional().is(professional) )
                .sorted(Comparator.comparing(MedicalCheck::getCreated))
                .collect(Collectors.toList());
    }

    public List<MedicalCheck> getAllChecks(Professional professional) {
        return this.repository.findAllByProfessional_Uuid(professional.getUuid()).stream()
                .filter(medicalCheck -> medicalCheck.getDate().after(new Date()))
                .collect(Collectors.toList());
    }
}