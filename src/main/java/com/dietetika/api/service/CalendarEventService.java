package com.dietetika.api.service;

import com.dietetika.api.model.calendar.CalendarEvent;
import com.dietetika.api.model.calendar.CalendarEventState;
import com.dietetika.api.model.calendar.CalendarEventType;
import com.dietetika.api.model.user.User;
import com.dietetika.api.repository.CalendarEventRepository;
import com.dietetika.api.util.logger.CommonLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class CalendarEventService {

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private CalendarEventRepository repository;

    public CalendarEvent create(User user, CalendarEvent event) {
        event.setUser( user );
        event.setState( CalendarEventState.CREATED.name() );
        event.setType( CalendarEventType.PERSONAL.name() );

        this.repository.save(event);

        try {
            this.notificationService.createByUser(user, event.getTitle(), event.getDescription(), event.getNotificationDate());
        } catch (Exception e) {
            CommonLogger.getLogger().error("Fail to create calendar notification", e);
        }

        return event;
    }

    public void update(User user, CalendarEvent changes) {
        CalendarEvent original = this.getCalendarEvent(user, changes.getId());
        original.setTitle( changes.getTitle() );
        original.setDescription( changes.getDescription() );
        original.setDueDate( changes.getDueDate() );
        original.setNotificationDate( changes.getNotificationDate() );
        original.setType( changes.getType() );
        original.setState( CalendarEventState.UPDATED.name() );
        original.setUpdated( new Date() );

        this.repository.save( original );
    }

    public void delete(User user, CalendarEvent changes) {
        CalendarEvent original = this.getCalendarEvent(user, changes.getId());
        original.setState( CalendarEventState.CANCELED.name() );
        original.setUpdated( new Date() );

        this.repository.save( original );
    }

    private CalendarEvent getCalendarEvent(User user, Long id) {
        return user.getCalendarEvent( id ).get();
    }
}