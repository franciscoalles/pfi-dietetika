package com.dietetika.api.service;

import com.dietetika.api.exception.PatientNoFoundException;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.model.user.User;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public abstract class UserService<T extends User> {

    protected abstract Map<String, T> getOnlineUsers();

    protected abstract Optional<T> searchInRepository(String id);

    protected abstract void save(T user);

    public void create(T user) {
        user.setUpdated(user.getCreated());
        this.save(user);
    }

    public Optional<T> getUser(String id) {
        Optional<T> op = Optional.ofNullable(this.getOnlineUsers().get(id));

        if (!op.isPresent()) {
            op = this.searchInRepository(id);
            this.add(op);
        }

        return op;
    }

    public void update(String id, T update) {
        T original = this.getUser( id ).get();
        original.setEmail(update.getEmail());
        original.setFirstName(update.getFirstName());
        original.setLastName(update.getLastName());
        original.setPhone(update.getPhone());
        this.save( original );
    }

    public void delete(String id) {
        T user = this.getUser(id).get();
        user.setDeleted(new Date());
        this.save(user);
        this.remove(user);
    }

    private void add(Optional<T> op) {
        if (!op.isPresent()) {
            T p = op.get();
            this.getOnlineUsers().put(p.getUuid(), p);
        }
    }

    private void remove(T user) {
        this.getOnlineUsers().remove(user.getUuid());
    }

    public abstract List<T> getAll();

    public void logout(String uuid) {
        this.getOnlineUsers().remove( uuid );
    }
}