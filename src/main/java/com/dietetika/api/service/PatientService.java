package com.dietetika.api.service;

import com.dietetika.api.exception.PatientNoFoundException;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.model.user.Professional;
import com.dietetika.api.repository.PatientRepository;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PatientService extends UserService<Patient> {

    @Autowired
    private PatientRepository repository;

    @Getter
    private Map<String, Patient> onlinePatients;

    public PatientService() {
        this.onlinePatients = new HashMap<>();
    }


    @Override
    protected Map<String, Patient> getOnlineUsers() {
        return this.getOnlinePatients();
    }

    @Override
    protected Optional<Patient> searchInRepository(String patientId) {
        Optional<Patient> op = this.repository.findByUuidAndDeletedIsNull(patientId);
        op.orElseThrow(PatientNoFoundException::new);
        return op;
    }

    @Override
    protected void save(Patient user) {
        this.repository.save( user );
    }

    @Override
    public List<Patient> getAll() {
        return this.repository.findByDeletedIsNull();
    }
}