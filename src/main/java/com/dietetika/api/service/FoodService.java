package com.dietetika.api.service;

import com.dietetika.api.model.food.Food;
import com.dietetika.api.model.tracking.FoodTracking;
import com.dietetika.api.model.tracking.RecognizedFood;
import com.dietetika.api.model.user.User;
import com.dietetika.api.repository.FoodRepository;
import com.dietetika.api.repository.FoodTrackingRepository;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class FoodService {

    @Autowired
    private FoodRepository repository;

    @Getter
    private Map<String, Food> foods;

    public FoodService() {
        this.foods = new HashMap<>();
    }

    public Optional<Food> getFood(String name) {
        if (this.foods.isEmpty())
            this.repository.findAll().forEach(food -> this.getFoods().put(food.getName().toLowerCase(), food));

        return Optional.ofNullable(this.getFoods().get(name.toLowerCase()));
    }

    // TODO complete implement food evaluation's functions
    public List<Food> getListOfFoods(String food) {
        List<Food> list = new ArrayList<>();

        Optional<Food> op = this.getFood(food);

        if (op.isPresent())
            list.add( op.get() );

        return list;
    }
}