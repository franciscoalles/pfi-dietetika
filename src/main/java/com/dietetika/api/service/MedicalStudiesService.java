package com.dietetika.api.service;

import com.dietetika.api.exception.DocumentCreationFailed;
import com.dietetika.api.exception.DocumentIncorrectNameCharset;
import com.dietetika.api.model.calendar.CalendarEvent;
import com.dietetika.api.model.calendar.CalendarEventState;
import com.dietetika.api.model.calendar.CalendarEventType;
import com.dietetika.api.model.document.DocumentStorageProperties;
import com.dietetika.api.model.document.MedicalStudy;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.model.user.User;
import com.dietetika.api.repository.CalendarEventRepository;
import com.dietetika.api.repository.MedicalStudiesRepository;
import com.dietetika.api.util.logger.CommonLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;

@Service
public class MedicalStudiesService {

    @Autowired
    private MedicalStudiesRepository repository;

    public MedicalStudy create(User user, MedicalStudy study) {
        study.setUser( user );
        study.setUpdated( new Date() );
//        this.storeFile(file, user.getId(), study.getType());
        this.repository.save(study);
        return study;
    }

    public void update(User user, MedicalStudy changes) {
        MedicalStudy original = this.getMedicalStudy(user, changes.getId());
        original.setDescription( changes.getDescription() );
        original.setType( changes.getType() );
        original.setNotes( changes.getNotes() );
        original.setUpdated( new Date() );
        this.repository.save( original );
    }

    public void delete(User user, MedicalStudy changes) {
        MedicalStudy original = this.getMedicalStudy(user, changes.getId());
        original.setUpdated( new Date() );
        this.repository.save( original );
    }

    private MedicalStudy getMedicalStudy(User user, Long id) {
        return ((Patient) user).getMedicalStudies( id ).get();
    }

    private DocumentStorageProperties storeFile(MultipartFile file, Long userId, String type) {
        // Normalize file name
        String originalFileName = StringUtils.cleanPath(file.getOriginalFilename());
        String fileName = "";
        try {
            // Check if the file's name contains invalid characters
            if(originalFileName.contains("..")) {
                throw new DocumentIncorrectNameCharset();
            }
            String fileExtension = "";
            try {
                fileExtension = originalFileName.substring(originalFileName.lastIndexOf("."));
            } catch(Exception e) {
                fileExtension = "";
            }
            fileName = userId + "_" + type + fileExtension;
            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = Paths.get("/home/dietetika/media/upload", file.getOriginalFilename());
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            DocumentStorageProperties newDoc = new DocumentStorageProperties();
            newDoc.setDocumentFormat(file.getContentType());
            newDoc.setFileName(fileName);

            return newDoc;
        } catch (IOException ex) {
            CommonLogger.getLogger().error(ex);
            throw new DocumentCreationFailed();
        }
    }
}