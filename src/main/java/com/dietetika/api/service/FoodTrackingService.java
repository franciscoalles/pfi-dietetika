package com.dietetika.api.service;

import com.dietetika.api.model.tracking.FoodTracking;
import com.dietetika.api.model.tracking.RecognizedFood;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.model.user.User;
import com.dietetika.api.repository.FoodTrackingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FoodTrackingService {

    @Autowired
    private FoodTrackingRepository repository;

    @Autowired
    private FoodService foodService;

    public FoodTracking create(User user, FoodTracking tracking) {
        tracking.setUser( user );
        tracking.setUpdated( new Date() );
        tracking.setRecognizedFoods( this.foundRecognizedFood(tracking.getName()) );
        this.repository.save(tracking);
        return tracking;
    }

    public void update(User user, FoodTracking changes) {
        FoodTracking original = this.getFoodTracking(user, changes.getId());
        if (!original.getName().equals( changes.getName() )) {
            original.setName( changes.getName() );
            original.setRecognizedFoods( this.foundRecognizedFood(changes.getName()) );
        }
        original.setUpdated( changes.getUpdated() );
        this.repository.save( original );
    }

    public void delete(User user, FoodTracking changes) {
        FoodTracking original = this.getFoodTracking(user, changes.getId());
        original.setDeleted( changes.getDeleted() );
        this.repository.save( original );
    }

    private FoodTracking getFoodTracking(User user, Long id) {
        return ((Patient) user).getFoodTracking( id ).get();
    }

    private List<RecognizedFood> foundRecognizedFood(String food) {
        return this.foodService.getListOfFoods(food).stream()
                .map(f -> RecognizedFood.builder()
                        .food(f)
                        .created(new Date())
                        .updated(new Date())
                        .build()
                ).collect(Collectors.toList());
    }
}