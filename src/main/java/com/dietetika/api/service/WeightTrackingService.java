package com.dietetika.api.service;

import com.dietetika.api.model.tracking.WeightTracking;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.model.user.User;
import com.dietetika.api.repository.WeightTrackingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class WeightTrackingService {

    @Autowired
    private WeightTrackingRepository repository;

    public WeightTracking create(User user, WeightTracking tracking) {
        tracking.setUser( user );
        tracking.setUpdated( new Date() );
        this.repository.save(tracking);
        return tracking;
    }

    public void update(User user, WeightTracking changes) {
        WeightTracking original = this.getWeightTracking(user, changes.getId());
        original.setWeight( changes.getWeight() );
        original.setUpdated( changes.getUpdated() );
        this.repository.save( original );
    }

    public void delete(User user, WeightTracking changes) {
        WeightTracking original = this.getWeightTracking(user, changes.getId());
        original.setDeleted( changes.getDeleted() );
        this.repository.save( original );
    }

    private WeightTracking getWeightTracking(User user, Long id) {
        return ((Patient) user).getWeightTracking( id ).get();
    }
}