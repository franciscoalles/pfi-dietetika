package com.dietetika.api.service;

import com.dietetika.api.exception.PatientNoFoundException;
import com.dietetika.api.exception.ProfessionalNoFoundException;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.model.user.Professional;
import com.dietetika.api.repository.ProfessionalRepository;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ProfessionalService extends UserService<Professional> {

    @Autowired
    private ProfessionalRepository repository;

    @Getter
    private Map<String, Professional> onlineProfessional;

    public ProfessionalService() {
        this.onlineProfessional = new HashMap<>();
    }


    @Override
    protected Map<String, Professional> getOnlineUsers() {
        return this.getOnlineProfessional();
    }

    @Override
    protected Optional<Professional> searchInRepository(String professionalId) {
        Optional<Professional> op = this.repository.findByUuidAndDeletedIsNull( professionalId );
        op.orElseThrow( ProfessionalNoFoundException::new );
        return op;
    }

    @Override
    protected void save(Professional user) {
        this.repository.save( user );
    }

    @Override
    public List<Professional> getAll() {
        return this.repository.findByDeletedIsNull();
    }
}