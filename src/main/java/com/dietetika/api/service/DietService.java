package com.dietetika.api.service;

import com.dietetika.api.model.diet.Diet;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.model.user.Professional;
import com.dietetika.api.model.user.User;
import com.dietetika.api.repository.DietRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class DietService {

    @Autowired
    private DietRepository repository;

    public List<Diet> getDiets(Professional professional) {
        return this.repository.findAllByProfessional_UuidOrProfessionalIsNull( professional.getUuid() );
    }

    public Diet getPatientDiet(Professional professional, Patient patient) {
        if (patient.getProfessional().is( professional ))
            return patient.getDiet();
        else
            return null;
    }
}