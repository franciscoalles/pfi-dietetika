package com.dietetika.api.service;

import com.dietetika.api.model.calendar.CalendarEventState;
import com.dietetika.api.model.calendar.CalendarEventType;
import com.dietetika.api.model.notification.Notification;
import com.dietetika.api.model.notification.NotificationType;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.model.user.User;
import com.dietetika.api.repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class NotificationService {

    @Autowired
    private NotificationRepository repository;

    public Notification createByUser(User user, String title, String description, Date notified) {
        Notification notification = new Notification();
        notification.setTitle( title );
        notification.setDescription( description );
        notification.setNotified( notified );
        notification.setUser( user );
        notification.setCreated( new Date() );
        notification.setType(
                user instanceof Patient ? NotificationType.PATIENT.toString() : NotificationType.PROFESSIONAL.toString()
        );

        this.repository.save(notification);

        return notification;
    }

    public void update(User user, Notification changes) {
        Notification original = this.getNotification(user, changes.getId());
        original.setSaw( changes.getSaw() );

        this.repository.save( original );
    }

    private Notification getNotification(User user, Long id) {
        return user.getNotification( id ).get();
    }
}