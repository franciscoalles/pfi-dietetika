package com.dietetika.api.controller.patient;

import com.dietetika.api.ObjectMapper;
import com.dietetika.api.dto.patient.calendar.*;
import com.dietetika.api.dto.patient.foodTracking.DeleteFoodTrackRequestDTO;
import com.dietetika.api.dto.patient.foodTracking.GetFoodTrackingByUserDTO;
import com.dietetika.api.dto.patient.foodTracking.ModifyFoodTrackRequestDTO;
import com.dietetika.api.dto.patient.foodTracking.RegisterFoodTrackRequestDTO;
import com.dietetika.api.model.tracking.FoodTracking;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.service.CalendarEventService;
import com.dietetika.api.service.FoodTrackingService;
import com.dietetika.api.service.PatientService;
import com.dietetika.api.util.logger.CommonLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/mobile/patient/food-tracking")
public class FoodTrackingController {

    @Autowired
    private FoodTrackingService foodTrackingService;

    @Autowired
    private PatientService patientService;

    @PutMapping("/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public CreateEventResponseDTO registerFoodTracking(@PathVariable String patientId, @RequestBody RegisterFoodTrackRequestDTO requestDTO) {
        CommonLogger.getLogger().info(requestDTO);
        FoodTracking event = this.foodTrackingService.create(this.getUser(patientId), ObjectMapper.map(requestDTO, FoodTracking.class));
        return CreateEventResponseDTO.builder().id( event.getId() ).build();
    }

    @PostMapping("/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public void modifyFoodTracking(@PathVariable String patientId, @RequestBody ModifyFoodTrackRequestDTO requestDTO) {
        CommonLogger.getLogger().info(requestDTO);
        this.foodTrackingService.update(this.getUser(patientId), ObjectMapper.map(requestDTO, FoodTracking.class));
    }

    @DeleteMapping("/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public void deleteFoodTracking(@PathVariable String patientId, @RequestBody DeleteFoodTrackRequestDTO requestDTO) {
        CommonLogger.getLogger().info(requestDTO);
        this.foodTrackingService.delete(this.getUser(patientId), ObjectMapper.map(requestDTO, FoodTracking.class));
    }

    @GetMapping("/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public List<GetFoodTrackingByUserDTO> getFoodTrackingByUser(@PathVariable String patientId) {
        CommonLogger.getLogger().info(patientId);
        return this.getUser(patientId).getFoodTrackings().stream()
                .map(f -> GetFoodTrackingByUserDTO.builder()
                        .id( f.getId() )
                        .name( f.getName() )
                        .updated( f.getUpdated() )
                        .kcal( f.getKCal() )
                        .build()
                ).collect(Collectors.toList());
    }

    private Patient getUser(String patientId) {
        return this.patientService.getUser(patientId).get();
    }
}