package com.dietetika.api.controller.patient;

import com.dietetika.api.ObjectMapper;
import com.dietetika.api.dto.patient.calendar.*;
import com.dietetika.api.model.calendar.CalendarEvent;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.service.CalendarEventService;
import com.dietetika.api.service.PatientService;
import com.dietetika.api.util.logger.CommonLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/mobile/patient/calendar")
public class CalendarPatientController {

    @Autowired
    private CalendarEventService calendarService;

    @Autowired
    private PatientService patientService;

    @PutMapping("/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public CreateEventResponseDTO createEvent(@PathVariable String patientId, @RequestBody CreateEventRequestDTO requestDTO) {
        CommonLogger.getLogger().info(requestDTO);
        CalendarEvent event = this.calendarService.create(this.getUser(patientId), ObjectMapper.map(requestDTO, CalendarEvent.class));
        return CreateEventResponseDTO.builder().id( event.getId() ).build();
    }

    @PostMapping("/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public void modifyEvent(@PathVariable String patientId, @RequestBody ModifyEventRequestDTO requestDTO) {
        CommonLogger.getLogger().info(requestDTO);
        this.calendarService.update(this.getUser(patientId), ObjectMapper.map(requestDTO, CalendarEvent.class));
    }

    @DeleteMapping("/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public void deleteEvent(@PathVariable String patientId, @RequestBody DeleteEventRequestDTO requestDTO) {
        CommonLogger.getLogger().info(requestDTO);
        this.calendarService.delete(this.getUser(patientId), ObjectMapper.map(requestDTO, CalendarEvent.class));
    }

    @GetMapping("/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public List<GetEventsByUserDTO> getEventsByUser(@PathVariable String patientId) {
        CommonLogger.getLogger().info(patientId);
        return ObjectMapper.mapAll(this.getUser(patientId).getFutureCalendarEvents(), GetEventsByUserDTO.class);
    }

    private Patient getUser(String patientId) {
        return this.patientService.getUser(patientId).get();
    }
}