package com.dietetika.api.controller.patient;

import com.dietetika.api.ObjectMapper;
import com.dietetika.api.dto.patient.calendar.CreateEventResponseDTO;
import com.dietetika.api.dto.patient.calendar.GetEventsByUserDTO;
import com.dietetika.api.dto.patient.foodTracking.GetFoodTrackingByUserDTO;
import com.dietetika.api.dto.patient.weightTracking.DeleteWeightTrackRequestDTO;
import com.dietetika.api.dto.patient.weightTracking.GetWeightTrackingByUserDTO;
import com.dietetika.api.dto.patient.weightTracking.ModifyWeightTrackRequestDTO;
import com.dietetika.api.dto.patient.weightTracking.RegisterWeightTrackRequestDTO;
import com.dietetika.api.model.tracking.FoodTracking;
import com.dietetika.api.model.tracking.WeightTracking;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.service.PatientService;
import com.dietetika.api.service.WeightTrackingService;
import com.dietetika.api.util.logger.CommonLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/mobile/patient/weight-tracking")
public class WeightTrackingController {

    @Autowired
    private WeightTrackingService weightTrackingService;

    @Autowired
    private PatientService patientService;

    @PutMapping("/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public CreateEventResponseDTO registerWeightTracking(@PathVariable String patientId, @RequestBody RegisterWeightTrackRequestDTO requestDTO) {
        CommonLogger.getLogger().info(requestDTO);
        WeightTracking event = this.weightTrackingService.create(this.getUser(patientId), ObjectMapper.map(requestDTO, WeightTracking.class));
        return CreateEventResponseDTO.builder().id( event.getId() ).build();
    }

    @PostMapping("/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public void modifyWeightTracking(@PathVariable String patientId, @RequestBody ModifyWeightTrackRequestDTO requestDTO) {
        CommonLogger.getLogger().info(requestDTO);
        this.weightTrackingService.update(this.getUser(patientId), ObjectMapper.map(requestDTO, WeightTracking.class));
    }

    @DeleteMapping("/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public void deleteWeightTracking(@PathVariable String patientId, @RequestBody DeleteWeightTrackRequestDTO requestDTO) {
        CommonLogger.getLogger().info(requestDTO);
        this.weightTrackingService.delete(this.getUser(patientId), ObjectMapper.map(requestDTO, WeightTracking.class));
    }

    @GetMapping("/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public List<GetWeightTrackingByUserDTO> getWeightTrackingByUser(@PathVariable String patientId) {
        CommonLogger.getLogger().info(patientId);
        return ObjectMapper.mapAll(this.getUser(patientId).getWeightTrackings(), GetWeightTrackingByUserDTO.class);
    }

    private Patient getUser(String patientId) {
        return this.patientService.getUser(patientId).get();
    }
}