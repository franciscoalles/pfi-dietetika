package com.dietetika.api.controller.patient;

import com.dietetika.api.ObjectMapper;
import com.dietetika.api.dto.patient.calendar.CreateEventResponseDTO;
import com.dietetika.api.dto.patient.medicalStudies.DeleteMedicalStudyRequestDTO;
import com.dietetika.api.dto.patient.medicalStudies.GetMedicalStudyByUserDTO;
import com.dietetika.api.dto.patient.medicalStudies.ModifyMedicalStudyRequestDTO;
import com.dietetika.api.dto.patient.medicalStudies.RegisterMedicalStudyRequestDTO;
import com.dietetika.api.dto.patient.weightTracking.DeleteWeightTrackRequestDTO;
import com.dietetika.api.dto.patient.weightTracking.GetWeightTrackingByUserDTO;
import com.dietetika.api.dto.patient.weightTracking.ModifyWeightTrackRequestDTO;
import com.dietetika.api.dto.patient.weightTracking.RegisterWeightTrackRequestDTO;
import com.dietetika.api.model.document.MedicalStudy;
import com.dietetika.api.model.tracking.WeightTracking;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.service.MedicalStudiesService;
import com.dietetika.api.service.PatientService;
import com.dietetika.api.service.WeightTrackingService;
import com.dietetika.api.util.logger.CommonLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping(path = "/mobile/patient/medical-studies")
public class MedicalStudiesController {

    @Autowired
    private MedicalStudiesService medicalStudiesService;

    @Autowired
    private PatientService patientService;

    @PutMapping("/{patientId}/")
    public CreateEventResponseDTO uploadFile(@PathVariable String patientId, @RequestBody RegisterMedicalStudyRequestDTO requestDTO) {

        CommonLogger.getLogger().info(requestDTO);
        MedicalStudy event = this.medicalStudiesService.create(this.getUser(patientId), ObjectMapper.map(requestDTO, MedicalStudy.class));
        return CreateEventResponseDTO.builder().id( event.getId() ).build();
    }

    @PostMapping("/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public void modifyWeightTracking(@PathVariable String patientId, @RequestBody ModifyMedicalStudyRequestDTO requestDTO) {
        CommonLogger.getLogger().info(requestDTO);
        this.medicalStudiesService.update(this.getUser(patientId), ObjectMapper.map(requestDTO, MedicalStudy.class));
    }

    @DeleteMapping("/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public void deleteWeightTracking(@PathVariable String patientId, @RequestBody DeleteMedicalStudyRequestDTO requestDTO) {
        CommonLogger.getLogger().info(requestDTO);
        this.medicalStudiesService.delete(this.getUser(patientId), ObjectMapper.map(requestDTO, MedicalStudy.class));
    }

    @GetMapping("/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public List<GetMedicalStudyByUserDTO> getWeightTrackingByUser(@PathVariable String patientId) {
        CommonLogger.getLogger().info(patientId);
        return ObjectMapper.mapAll(this.getUser(patientId).getMedicalStudies(), GetMedicalStudyByUserDTO.class);
    }

    private Patient getUser(String patientId) {
        return this.patientService.getUser(patientId).get();
    }
}