package com.dietetika.api.controller.patient;

import com.dietetika.api.ObjectMapper;
import com.dietetika.api.dto.patient.calendar.CreateEventResponseDTO;
import com.dietetika.api.dto.patient.weightTracking.DeleteWeightTrackRequestDTO;
import com.dietetika.api.dto.patient.weightTracking.GetWeightTrackingByUserDTO;
import com.dietetika.api.dto.patient.weightTracking.ModifyWeightTrackRequestDTO;
import com.dietetika.api.dto.patient.weightTracking.RegisterWeightTrackRequestDTO;
import com.dietetika.api.dto.professional.GetProfessionalDTO;
import com.dietetika.api.model.tracking.WeightTracking;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.service.PatientService;
import com.dietetika.api.service.ProfessionalService;
import com.dietetika.api.service.WeightTrackingService;
import com.dietetika.api.util.logger.CommonLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/mobile/patient/professional")
public class ProfessionalController {

    @Autowired
    private ProfessionalService professionalService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<GetProfessionalDTO> getAllProfessionals() {
        CommonLogger.getLogger().info("getAllProfessionals");
        return ObjectMapper.mapAll(this.professionalService.getAll(), GetProfessionalDTO.class);
    }
}