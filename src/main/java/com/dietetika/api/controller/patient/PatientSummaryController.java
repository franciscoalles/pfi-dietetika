package com.dietetika.api.controller.patient;

import com.dietetika.api.ObjectMapper;
import com.dietetika.api.dto.patient.GetNextCheckDTO;
import com.dietetika.api.dto.patient.calendar.CreateEventResponseDTO;
import com.dietetika.api.dto.patient.medicalStudies.DeleteMedicalStudyRequestDTO;
import com.dietetika.api.dto.patient.medicalStudies.GetMedicalStudyByUserDTO;
import com.dietetika.api.dto.patient.medicalStudies.ModifyMedicalStudyRequestDTO;
import com.dietetika.api.dto.patient.medicalStudies.RegisterMedicalStudyRequestDTO;
import com.dietetika.api.dto.professional.diet.GetDietsByUserDTO;
import com.dietetika.api.model.document.MedicalStudy;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.service.MedicalCheckService;
import com.dietetika.api.service.MedicalStudiesService;
import com.dietetika.api.service.PatientService;
import com.dietetika.api.util.logger.CommonLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(path = "/mobile/patient/summary")
public class PatientSummaryController {

    @Autowired
    private PatientService patientService;

    @GetMapping("/nextCheck/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public GetNextCheckDTO getNextCheck(@PathVariable String patientId) {
        CommonLogger.getLogger().info("nextCheck -> " + patientId);
        Date nextCheckDate = this.getUser(patientId).getNextCheck();

        if (nextCheckDate != null) {
            Long nextCheck = Duration.between(new Date().toInstant(), nextCheckDate.toInstant()).toDays();
            return GetNextCheckDTO.builder()
                    .nextCheck( nextCheck )
                    .nextCheckDate( nextCheckDate )
                    .build();
        } else {
            return GetNextCheckDTO.builder()
                    .nextCheck( 0l )
                    .nextCheckDate( null )
                    .build();
        }
    }

    @GetMapping("/diet/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public GetDietsByUserDTO getDiet(@PathVariable String patientId) {
        CommonLogger.getLogger().info("getDiet -> " + patientId);
        return ObjectMapper.map(this.getUser(patientId).getDiet(), GetDietsByUserDTO.class);
    }

    private Patient getUser(String patientId) {
        return this.patientService.getUser(patientId).get();
    }
}