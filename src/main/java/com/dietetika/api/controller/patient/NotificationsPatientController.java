package com.dietetika.api.controller.patient;

import com.dietetika.api.ObjectMapper;
import com.dietetika.api.dto.notification.GetOldNotificationsByUserDTO;
import com.dietetika.api.dto.notification.SawNotificationRequestDTO;
import com.dietetika.api.dto.patient.calendar.*;
import com.dietetika.api.model.calendar.CalendarEvent;
import com.dietetika.api.model.notification.Notification;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.service.CalendarEventService;
import com.dietetika.api.service.NotificationService;
import com.dietetika.api.service.PatientService;
import com.dietetika.api.util.logger.CommonLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/mobile/patient/notifications")
public class NotificationsPatientController {

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private PatientService patientService;

    @PostMapping("/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public void markedLikeSaw(@PathVariable String patientId, @RequestBody SawNotificationRequestDTO requestDTO) {
        CommonLogger.getLogger().info(requestDTO);
        this.notificationService.update(this.getUser(patientId), ObjectMapper.map(requestDTO, Notification.class));
    }

    @GetMapping("/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public List<GetOldNotificationsByUserDTO> getNotificationByUser(@PathVariable String patientId) {
        CommonLogger.getLogger().info(patientId);
        return ObjectMapper.mapAll(this.getUser(patientId).getOldNotifications(), GetOldNotificationsByUserDTO.class);
    }

    private Patient getUser(String patientId) {
        return this.patientService.getUser(patientId).get();
    }
}