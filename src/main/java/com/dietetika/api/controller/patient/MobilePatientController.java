package com.dietetika.api.controller.patient;

import com.dietetika.api.ObjectMapper;
import com.dietetika.api.dto.patient.CreatePatientUserDTO;
import com.dietetika.api.dto.patient.UpdatePatientUserDTO;
import com.dietetika.api.dto.signInMobilePatient.SignInMobilePatientRequestDTO;
import com.dietetika.api.dto.signInMobilePatient.SignInMobilePatientResponseDTO;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.model.user.Professional;
import com.dietetika.api.service.PatientService;
import com.dietetika.api.util.logger.CommonLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/mobile/patient/session/")
public class MobilePatientController {

    @Autowired
    private PatientService patientService;

    @GetMapping
    public String getStatus() {
        return "OK";
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public void createPatientUser(@RequestBody CreatePatientUserDTO requestDTO) {
        this.patientService.create(ObjectMapper.map(requestDTO, Patient.class));
    }

    @PostMapping("/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public void updatePatientUser(@PathVariable String patientId, @RequestBody UpdatePatientUserDTO requestDTO) {
        this.patientService.update(patientId, ObjectMapper.map(requestDTO, Patient.class));
    }

    @DeleteMapping("/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public void deletePatientUser(@PathVariable String patientId) {
        this.patientService.delete(patientId);
    }

    @PostMapping("/login")
    @ResponseStatus(HttpStatus.OK)
    public SignInMobilePatientResponseDTO signIn(@RequestBody SignInMobilePatientRequestDTO requestDTO) {
        CommonLogger.getLogger().info(requestDTO);
        Patient p = this.patientService.getUser(requestDTO.getUuid()).get();
        return SignInMobilePatientResponseDTO.builder()
                .firstName( p.getFirstName() )
                .lastName( p.getLastName() )
                .phone( p.getPhone() )
                .build();
    }

    @PostMapping("/logout")
    @ResponseStatus(HttpStatus.OK)
    public void signOut(@RequestBody SignInMobilePatientRequestDTO requestDTO) {
        CommonLogger.getLogger().info(requestDTO);
        this.patientService.logout(requestDTO.getUuid());
    }
}