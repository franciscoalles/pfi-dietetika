package com.dietetika.api.controller.professional;

import com.dietetika.api.ObjectMapper;
import com.dietetika.api.dto.patient.calendar.CreateEventResponseDTO;
import com.dietetika.api.dto.patient.foodTracking.DeleteFoodTrackRequestDTO;
import com.dietetika.api.dto.patient.foodTracking.GetFoodTrackingByUserDTO;
import com.dietetika.api.dto.patient.foodTracking.ModifyFoodTrackRequestDTO;
import com.dietetika.api.dto.patient.foodTracking.RegisterFoodTrackRequestDTO;
import com.dietetika.api.model.tracking.FoodTracking;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.service.FoodTrackingService;
import com.dietetika.api.service.PatientService;
import com.dietetika.api.util.logger.CommonLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/mobile/professional/food-tracking")
public class FoodTrackingProfessionalController {

    @Autowired
    private FoodTrackingService foodTrackingService;

    @Autowired
    private PatientService patientService;

    @GetMapping("/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public List<GetFoodTrackingByUserDTO> getFoodTrackingByUser(@PathVariable String patientId) {
        CommonLogger.getLogger().info(patientId);
        return this.getUser(patientId).getFoodTrackings().stream()
                .map(f -> GetFoodTrackingByUserDTO.builder()
                        .id( f.getId() )
                        .name( f.getName() )
                        .updated( f.getUpdated() )
                        .kcal( f.getKCal() )
                        .build()
                ).collect(Collectors.toList());
    }

    private Patient getUser(String patientId) {
        return this.patientService.getUser(patientId).get();
    }
}