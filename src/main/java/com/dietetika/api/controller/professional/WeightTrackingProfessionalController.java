package com.dietetika.api.controller.professional;

import com.dietetika.api.ObjectMapper;
import com.dietetika.api.dto.patient.calendar.CreateEventResponseDTO;
import com.dietetika.api.dto.patient.weightTracking.DeleteWeightTrackRequestDTO;
import com.dietetika.api.dto.patient.weightTracking.GetWeightTrackingByUserDTO;
import com.dietetika.api.dto.patient.weightTracking.ModifyWeightTrackRequestDTO;
import com.dietetika.api.dto.patient.weightTracking.RegisterWeightTrackRequestDTO;
import com.dietetika.api.model.tracking.WeightTracking;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.service.PatientService;
import com.dietetika.api.service.WeightTrackingService;
import com.dietetika.api.util.logger.CommonLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/mobile/professional/weight-tracking")
public class WeightTrackingProfessionalController {

    @Autowired
    private WeightTrackingService weightTrackingService;

    @Autowired
    private PatientService patientService;

    @GetMapping("/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public List<GetWeightTrackingByUserDTO> getWeightTrackingByUser(@PathVariable String patientId) {
        CommonLogger.getLogger().info(patientId);
        return ObjectMapper.mapAll(this.getUser(patientId).getWeightTrackings(), GetWeightTrackingByUserDTO.class);
    }

    private Patient getUser(String patientId) {
        return this.patientService.getUser(patientId).get();
    }
}