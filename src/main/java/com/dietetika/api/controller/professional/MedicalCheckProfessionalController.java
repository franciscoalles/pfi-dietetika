package com.dietetika.api.controller.professional;

import com.dietetika.api.ObjectMapper;
import com.dietetika.api.dto.patient.GetChecksWithPatientDTO;
import com.dietetika.api.dto.patient.GetPatientsChecksDTO;
import com.dietetika.api.dto.patient.GetPatientsNotesDTO;
import com.dietetika.api.dto.professional.check.CreateCheckRequestDTO;
import com.dietetika.api.dto.professional.check.CreateCheckResponseDTO;
import com.dietetika.api.dto.professional.note.CreateNoteRequestDTO;
import com.dietetika.api.dto.professional.note.CreateNoteResponseDTO;
import com.dietetika.api.model.check.MedicalCheck;
import com.dietetika.api.model.check.Note;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.model.user.Professional;
import com.dietetika.api.service.MedicalCheckService;
import com.dietetika.api.service.NoteService;
import com.dietetika.api.service.PatientService;
import com.dietetika.api.service.ProfessionalService;
import com.dietetika.api.util.logger.CommonLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/mobile/professional/medical-check")
public class MedicalCheckProfessionalController {

    @Autowired
    private ProfessionalService professionalService;

    @Autowired
    private PatientService patientService;

    @Autowired
    private MedicalCheckService medicalCheckService;

    @PutMapping("/{professionalId}/")
    @ResponseStatus(HttpStatus.OK)
    public CreateCheckResponseDTO createEvent(@PathVariable String professionalId, @RequestBody CreateCheckRequestDTO requestDTO) {
        CommonLogger.getLogger().info(requestDTO);
        MedicalCheck check = this.medicalCheckService.create(
                this.getProfessionalUser(professionalId),
                this.getPatientUser(requestDTO.getPatient()),
                ObjectMapper.map(requestDTO.getCheck(), MedicalCheck.class)
        );
        return CreateCheckResponseDTO.builder().id( check.getId() ).build();
    }
    
    @GetMapping("/{professionalId}/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public List<GetPatientsChecksDTO> getPatientChecks(@PathVariable String professionalId,
                                                       @PathVariable String patientId) {
        CommonLogger.getLogger().info("getPatientChecks - Prof:" + professionalId + " - patient: " + patientId);
        return ObjectMapper.mapAll(
                this.medicalCheckService.getPatientChecks(this.getProfessionalUser(professionalId), this.getPatientUser(patientId)),
                GetPatientsChecksDTO.class
        );
    }

    @GetMapping("/{professionalId}/")
    @ResponseStatus(HttpStatus.OK)
    public List<GetChecksWithPatientDTO> getAllChecks(@PathVariable String professionalId) {
        CommonLogger.getLogger().info("getAllChecks - Prof:" + professionalId);
        return ObjectMapper.mapAll(this.medicalCheckService.getAllChecks(this.getProfessionalUser(professionalId)), GetChecksWithPatientDTO.class);
    }

    private Professional getProfessionalUser(String professionalId) {
        return this.professionalService.getUser(professionalId).get();
    }

    private Patient getPatientUser(String patientId) {
        return this.patientService.getUser(patientId).get();
    }
}