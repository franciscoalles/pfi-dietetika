package com.dietetika.api.controller.professional;

import com.dietetika.api.ObjectMapper;
import com.dietetika.api.dto.patient.CreatePatientUserDTO;
import com.dietetika.api.dto.professional.CreateProfessionalUserDTO;
import com.dietetika.api.dto.professional.UpdateProfessionalUserDTO;
import com.dietetika.api.dto.signInMobilePatient.SignInMobilePatientRequestDTO;
import com.dietetika.api.dto.signInMobilePatient.SignInMobilePatientResponseDTO;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.model.user.Professional;
import com.dietetika.api.service.PatientService;
import com.dietetika.api.service.ProfessionalService;
import com.dietetika.api.util.logger.CommonLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/mobile/professional/session/")
public class MobileProfessionalController {

    @Autowired
    private ProfessionalService professionalService;

    @GetMapping
    public String getStatus() {
        return "OK";
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public void createProfessionalUser(@RequestBody CreateProfessionalUserDTO requestDTO) {
        this.professionalService.create(ObjectMapper.map(requestDTO, Professional.class));
    }

    @PostMapping("/{professionalId}/")
    @ResponseStatus(HttpStatus.OK)
    public void updateProfessionalUser(@PathVariable String professionalId, UpdateProfessionalUserDTO requestDTO) {
        this.professionalService.update(professionalId, ObjectMapper.map(requestDTO, Professional.class));
    }

    @DeleteMapping("/{professionalId}/")
    @ResponseStatus(HttpStatus.OK)
    public void deleteProfessionalUser(@PathVariable String professionalId) {
        this.professionalService.delete(professionalId);
    }

    @PostMapping("/login")
    @ResponseStatus(HttpStatus.OK)
    public SignInMobilePatientResponseDTO signIn(@RequestBody SignInMobilePatientRequestDTO requestDTO) {
        CommonLogger.getLogger().info(requestDTO);
        Professional p = this.professionalService.getUser(requestDTO.getUuid()).get();
        return SignInMobilePatientResponseDTO.builder()
                .firstName( p.getFirstName() )
                .lastName( p.getLastName() )
                .phone( p.getPhone() )
                .build();
    }

    @PostMapping("/logout")
    @ResponseStatus(HttpStatus.OK)
    public void signOut(@RequestBody SignInMobilePatientRequestDTO requestDTO) {
        CommonLogger.getLogger().info(requestDTO);
        this.professionalService.logout(requestDTO.getUuid());
    }
}