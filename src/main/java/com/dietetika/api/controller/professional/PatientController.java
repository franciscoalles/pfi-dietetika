package com.dietetika.api.controller.professional;

import com.dietetika.api.ObjectMapper;
import com.dietetika.api.dto.patient.GetPatientsFollowsDTO;
import com.dietetika.api.dto.patient.foodTracking.GetFoodTrackingByUserDTO;
import com.dietetika.api.dto.professional.GetProfessionalDTO;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.model.user.Professional;
import com.dietetika.api.service.ProfessionalService;
import com.dietetika.api.util.logger.CommonLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/mobile/professional/patient")
public class PatientController {

    @Autowired
    private ProfessionalService professionalService;

    @GetMapping("/{professionalId}/")
    @ResponseStatus(HttpStatus.OK)
    public List<GetPatientsFollowsDTO> getAllPatientsByProfessional(@PathVariable String professionalId) {
        CommonLogger.getLogger().info("getAllPatientsByProfessional " + professionalId);

        return this.getUser(professionalId).getFollows().stream()
                .map(p -> GetPatientsFollowsDTO.builder()
                        .id( p.getId() )
                        .firstName( p.getFirstName() )
                        .lastName( p.getLastName() )
                        .email( p.getEmail() )
                        .lastCheck( p.getLastCheck() )
                        .uuid( p.getUuid() )
                        .build()
                ).collect(Collectors.toList());
    }

    private Professional getUser(String professionalId) {
        return this.professionalService.getUser(professionalId).get();
    }
}