package com.dietetika.api.controller.professional;

import com.dietetika.api.ObjectMapper;
import com.dietetika.api.dto.patient.calendar.CreateEventResponseDTO;
import com.dietetika.api.dto.patient.medicalStudies.DeleteMedicalStudyRequestDTO;
import com.dietetika.api.dto.patient.medicalStudies.GetMedicalStudyByUserDTO;
import com.dietetika.api.dto.patient.medicalStudies.ModifyMedicalStudyRequestDTO;
import com.dietetika.api.dto.patient.medicalStudies.RegisterMedicalStudyRequestDTO;
import com.dietetika.api.model.document.MedicalStudy;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.service.MedicalStudiesService;
import com.dietetika.api.service.PatientService;
import com.dietetika.api.util.logger.CommonLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/mobile/professional/medical-studies")
public class MedicalStudiesProfessionalController {

    @Autowired
    private MedicalStudiesService medicalStudiesService;

    @Autowired
    private PatientService patientService;

    @GetMapping("/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public List<GetMedicalStudyByUserDTO> getWeightTrackingByUser(@PathVariable String patientId) {
        CommonLogger.getLogger().info(patientId);
        return ObjectMapper.mapAll(this.getUser(patientId).getMedicalStudies(), GetMedicalStudyByUserDTO.class);
    }

    private Patient getUser(String patientId) {
        return this.patientService.getUser(patientId).get();
    }
}