package com.dietetika.api.controller.professional;

import com.dietetika.api.ObjectMapper;
import com.dietetika.api.dto.patient.calendar.*;
import com.dietetika.api.dto.professional.diet.GetDietsByUserDTO;
import com.dietetika.api.dto.professional.diet.RegisterDietRequestDTO;
import com.dietetika.api.model.diet.Diet;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.model.user.Professional;
import com.dietetika.api.service.DietService;
import com.dietetika.api.service.PatientService;
import com.dietetika.api.service.ProfessionalService;
import com.dietetika.api.util.logger.CommonLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/mobile/professional/diet")
public class DietProfessionalController {

    @Autowired
    private DietService dietService;

    @Autowired
    private ProfessionalService professionalService;

    @Autowired
    private PatientService patientService;

    @GetMapping("/{professionalId}/")
    @ResponseStatus(HttpStatus.OK)
    public List<GetDietsByUserDTO> getEventsByUser(@PathVariable String professionalId) {
        CommonLogger.getLogger().info(professionalId);
        return ObjectMapper.mapAll(this.dietService.getDiets( this.getProfessionalUser(professionalId) ), GetDietsByUserDTO.class);
    }

    @GetMapping("/{professionalId}/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public GetDietsByUserDTO getPatientDiet(@PathVariable String professionalId, @PathVariable String patientId) {
        CommonLogger.getLogger().info(patientId);
        return ObjectMapper.map(
                this.dietService.getPatientDiet( this.getProfessionalUser(professionalId),this.getPatientUser(patientId)),
                GetDietsByUserDTO.class);
    }

    private Professional getProfessionalUser(String professionalId) {
        return this.professionalService.getUser(professionalId).get();
    }

    private Patient getPatientUser(String patientId) {
        return this.patientService.getUser(patientId).get();
    }
}