package com.dietetika.api.controller.professional;

import com.dietetika.api.ObjectMapper;
import com.dietetika.api.dto.patient.GetPatientsFollowsDTO;
import com.dietetika.api.dto.patient.GetPatientsNotesDTO;
import com.dietetika.api.dto.patient.calendar.CreateEventRequestDTO;
import com.dietetika.api.dto.patient.calendar.CreateEventResponseDTO;
import com.dietetika.api.dto.patient.weightTracking.GetWeightTrackingByUserDTO;
import com.dietetika.api.dto.professional.UpdateProfessionalUserDTO;
import com.dietetika.api.dto.professional.note.CreateNoteRequestDTO;
import com.dietetika.api.dto.professional.note.CreateNoteResponseDTO;
import com.dietetika.api.model.calendar.CalendarEvent;
import com.dietetika.api.model.check.Note;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.model.user.Professional;
import com.dietetika.api.service.NoteService;
import com.dietetika.api.service.PatientService;
import com.dietetika.api.service.ProfessionalService;
import com.dietetika.api.util.logger.CommonLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/mobile/professional/notes")
public class NotesProfessionalController {

    @Autowired
    private ProfessionalService professionalService;

    @Autowired
    private PatientService patientService;

    @Autowired
    private NoteService noteService;

    @PutMapping("/{professionalId}/")
    @ResponseStatus(HttpStatus.OK)
    public CreateNoteResponseDTO createEvent(@PathVariable String professionalId, @RequestBody CreateNoteRequestDTO requestDTO) {
        CommonLogger.getLogger().info(requestDTO);
        Note note = this.noteService.create(
                this.getProfessionalUser(professionalId),
                this.getPatientUser(requestDTO.getPatient()),
                ObjectMapper.map(requestDTO.getNote(), Note.class)
        );
        return CreateNoteResponseDTO.builder().id( note.getId() ).build();
    }

    @GetMapping("/{professionalId}/{patientId}/")
    @ResponseStatus(HttpStatus.OK)
    public List<GetPatientsNotesDTO> getPatientNotes(@PathVariable String professionalId,
                                                     @PathVariable String patientId) {
        CommonLogger.getLogger().info("getPatientNotes - Prof:" + professionalId + " - patient: " + patientId);
        return ObjectMapper.mapAll(
                this.noteService.getPatientNotes(this.getProfessionalUser(professionalId), this.getPatientUser(patientId)),
                GetPatientsNotesDTO.class
        );
    }

    private Professional getProfessionalUser(String professionalId) {
        return this.professionalService.getUser(professionalId).get();
    }

    private Patient getPatientUser(String patientId) {
        return this.patientService.getUser(patientId).get();
    }
}