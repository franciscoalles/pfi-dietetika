package com.dietetika.api.controller.professional;

import com.dietetika.api.ObjectMapper;
import com.dietetika.api.dto.patient.calendar.*;
import com.dietetika.api.model.calendar.CalendarEvent;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.model.user.Professional;
import com.dietetika.api.service.CalendarEventService;
import com.dietetika.api.service.PatientService;
import com.dietetika.api.service.ProfessionalService;
import com.dietetika.api.util.logger.CommonLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/mobile/professional/calendar")
public class CalendarProfessionalController {

    @Autowired
    private CalendarEventService calendarService;

    @Autowired
    private ProfessionalService professionalService;

    @PutMapping("/{professionalId}/")
    @ResponseStatus(HttpStatus.OK)
    public CreateEventResponseDTO createEvent(@PathVariable String professionalId, @RequestBody CreateEventRequestDTO requestDTO) {
        CommonLogger.getLogger().info(requestDTO);
        CalendarEvent event = this.calendarService.create(this.getUser(professionalId), ObjectMapper.map(requestDTO, CalendarEvent.class));
        return CreateEventResponseDTO.builder().id( event.getId() ).build();
    }

    @PostMapping("/{professionalId}/")
    @ResponseStatus(HttpStatus.OK)
    public void modifyEvent(@PathVariable String professionalId, @RequestBody ModifyEventRequestDTO requestDTO) {
        CommonLogger.getLogger().info(requestDTO);
        this.calendarService.update(this.getUser(professionalId), ObjectMapper.map(requestDTO, CalendarEvent.class));
    }

    @DeleteMapping("/{professionalId}/")
    @ResponseStatus(HttpStatus.OK)
    public void deleteEvent(@PathVariable String professionalId, @RequestBody DeleteEventRequestDTO requestDTO) {
        CommonLogger.getLogger().info(requestDTO);
        this.calendarService.delete(this.getUser(professionalId), ObjectMapper.map(requestDTO, CalendarEvent.class));
    }

    @GetMapping("/{professionalId}/")
    @ResponseStatus(HttpStatus.OK)
    public List<GetEventsByUserDTO> getEventsByUser(@PathVariable String professionalId) {
        CommonLogger.getLogger().info(professionalId);
        return ObjectMapper.mapAll(this.getUser(professionalId).getCalendarEvents(), GetEventsByUserDTO.class);
    }

    private Professional getUser(String professionalId) {
        return this.professionalService.getUser(professionalId).get();
    }
}