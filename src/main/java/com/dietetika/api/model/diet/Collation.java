package com.dietetika.api.model.diet;

import com.dietetika.api.model.food.Food;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class Collation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String moment;
    private String name;
    private String description;
    private Date created;
    private Date updated;
    private Date deleted;
    private Integer day;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Food food;
}