package com.dietetika.api.model.diet;

public enum CollationMoment {
    MORNING, MID_MORNING, LUNCH, MID_AFTERNOON, AFTERNOON, DINNER
}