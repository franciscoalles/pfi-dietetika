package com.dietetika.api.model.diet;

import com.dietetika.api.model.user.Patient;
import com.dietetika.api.model.user.Professional;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Data
public class Diet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    private Date created;
    private Date updated;
    private Date deleted;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Collation> collations;

    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "professional_id", nullable = true, updatable = false)
    private Professional professional;

    public boolean hasId(Long otherId) {
        return this.getId().equals( otherId );
    }
}
