package com.dietetika.api.model.check;

import com.dietetika.api.model.user.Patient;
import com.dietetika.api.model.user.Professional;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "medical_check")
@Data
@NoArgsConstructor(force = true)
public class MedicalCheck {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Date created;
    private Date date;
    private String address;
    private String note;
    private Date updated;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "patient_id", nullable = false, updatable = false)
    private Patient patient;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "professional_id", nullable = false, updatable = false)
    private Professional professional;

    public boolean hasId(Long otherId) {
        return this.getId().equals( otherId );
    }
}