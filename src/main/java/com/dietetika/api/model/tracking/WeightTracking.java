package com.dietetika.api.model.tracking;

import com.dietetika.api.model.user.User;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Entity
@Data
@NoArgsConstructor
public class WeightTracking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Float weight;

    private Date created;

    private Date updated;

    private Date deleted;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false, updatable = false)
    private User user;

    public boolean hasId(Long otherId) {
        return this.getId().equals( otherId );
    }
}