package com.dietetika.api.model.tracking;

import com.dietetika.api.model.user.User;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Entity
@Data
@NoArgsConstructor
public class FoodTracking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(cascade = CascadeType.ALL)
    private List<RecognizedFood> recognizedFoods;

    private Date created;

    private Date updated;

    private Date deleted;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false, updatable = false)
    private User user;

    public boolean hasId(Long otherId) {
        return this.getId().equals( otherId );
    }

    public float getKCal() {
        AtomicReference<Float> sum = new AtomicReference<>((float) 0);
        this.getRecognizedFoods().stream()
                .forEach(rf -> sum.updateAndGet(v -> v + rf.getFood().getCaloriesEachGram() ));
        return sum.get();
    }
}