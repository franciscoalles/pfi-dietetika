package com.dietetika.api.model.notification;

public enum NotificationType {
    PATIENT, PROFESSIONAL, SYSTEM
}
