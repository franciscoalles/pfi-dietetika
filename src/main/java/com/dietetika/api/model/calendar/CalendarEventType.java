package com.dietetika.api.model.calendar;

public enum CalendarEventType {
    PERSONAL, MEDIC, FITNESS, ADVICE
}