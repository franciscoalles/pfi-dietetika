package com.dietetika.api.model.calendar;

import com.dietetika.api.model.user.User;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "calendar_event")
@Data
@NoArgsConstructor(force = true)
// @Builder
public class CalendarEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String title;
    private String description;
    private String type;
    private Date created;
    private String state;
    private Date dueDate;
    private Date notificationDate;
    private Date updated;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false, updatable = false)
    private User user;

    public boolean hasId(Long otherId) {
        return this.getId().equals( otherId );
    }
}