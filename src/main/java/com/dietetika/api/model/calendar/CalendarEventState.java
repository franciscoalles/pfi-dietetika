package com.dietetika.api.model.calendar;

public enum CalendarEventState {
    CREATED, UPDATED, NOTIFIED, COMPLETED, CANCELED
}