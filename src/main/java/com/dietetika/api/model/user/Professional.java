package com.dietetika.api.model.user;

import com.dietetika.api.exception.DietNotFound;
import com.dietetika.api.exception.MedicalStudyNotFound;
import com.dietetika.api.model.calendar.CalendarEvent;
import com.dietetika.api.model.diet.Diet;
import com.dietetika.api.model.document.MedicalStudy;
import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Entity
@DiscriminatorValue("PROFESSIONAL")
@Data
public class Professional extends User implements Serializable {

    private String type;
    private String description;
    private Integer yearsExperience;
    private String urlPicture;
    private String medicalId;
    private boolean premium;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "professional")
    private List<Diet> diets;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Patient> follows;

    public Optional<Diet> getDiets(Long dietId) {
        return Optional.ofNullable(this.diets.stream()
                .filter(diet -> diet.hasId(dietId))
                .findFirst().orElseThrow(() -> new DietNotFound()));
    }
}
