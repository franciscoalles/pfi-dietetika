package com.dietetika.api.model.user;

import com.dietetika.api.exception.FoodTrackingNotFound;
import com.dietetika.api.exception.MedicalStudyNotFound;
import com.dietetika.api.exception.WeightTrackingNotFound;
import com.dietetika.api.model.check.MedicalCheck;
import com.dietetika.api.model.check.Note;
import com.dietetika.api.model.diet.Diet;
import com.dietetika.api.model.document.MedicalStudy;
import com.dietetika.api.model.tracking.FoodTracking;
import com.dietetika.api.model.tracking.WeightTracking;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Entity
@DiscriminatorValue("PATIENT")
@Data
public class Patient extends User implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<FoodTracking> foodTrackings;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<WeightTracking> weightTrackings;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<MedicalStudy> medicalStudies;

    @OneToOne(cascade = CascadeType.ALL)
    private Professional professional;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "patient")
    private List<MedicalCheck> medicalChecks;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "patient")
    private List<Note> notes;

    @ManyToOne(cascade = CascadeType.ALL)
    private Diet diet;

    public Optional<FoodTracking> getFoodTracking(Long trackingId) {
        return Optional.ofNullable(this.foodTrackings.stream()
                .filter(tracking -> tracking.hasId(trackingId))
                .findFirst().orElseThrow(() -> new FoodTrackingNotFound()));
    }

    public Optional<WeightTracking> getWeightTracking(Long trackingId) {
        return Optional.ofNullable(this.weightTrackings.stream()
                .filter(tracking -> tracking.hasId(trackingId))
                .findFirst().orElseThrow(() -> new WeightTrackingNotFound()));
    }

    public Optional<MedicalStudy> getMedicalStudies(Long studyId) {
        return Optional.ofNullable(this.medicalStudies.stream()
                .filter(study -> study.hasId(studyId))
                .findFirst().orElseThrow(() -> new MedicalStudyNotFound()));
    }

    public Date getNextCheck() {
        Optional<Date> op = this.medicalChecks.stream()
                .filter(medicalCheck -> medicalCheck.getDate().after(new Date()))
                .map(MedicalCheck::getDate).min(Date::compareTo);
        return (op.isPresent() ? op.get() : null);
    }

    public Date getLastCheck() {
        Optional<Date> op = this.medicalChecks.stream()
                .filter(medicalCheck -> medicalCheck.getDate().before(new Date()))
                .map(MedicalCheck::getDate).max(Date::compareTo);
        return (op.isPresent() ? op.get() : null);
    }
}
