package com.dietetika.api.model.user;

import com.dietetika.api.exception.*;
import com.dietetika.api.model.calendar.CalendarEvent;
import com.dietetika.api.model.document.MedicalStudy;
import com.dietetika.api.model.notification.Notification;
import com.dietetika.api.model.tracking.FoodTracking;
import com.dietetika.api.model.tracking.WeightTracking;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(
        name="type",
        discriminatorType = DiscriminatorType.STRING,
        columnDefinition = "VARCHAR(15)"
)
@Table(name="user")
@Data
public abstract class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String uuid;
    private String email;
    private String phone;
    private Date created;
    private Date updated;
    private String firstName;
    private String lastName;
    // private String type;
    private Date deleted;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<CalendarEvent> calendarEvents;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<Notification> notifications;

    public User() {
        this.calendarEvents = new ArrayList<>();
    }

    public Optional<CalendarEvent> getCalendarEvent(Long calendarId) {
        return Optional.ofNullable(this.calendarEvents.stream()
                .filter(calendarEvent -> calendarEvent.hasId(calendarId))
                .findFirst().orElseThrow(() -> new CalendarEventNotFound()));
    }

    public List<CalendarEvent> getFutureCalendarEvents() {
        return this.calendarEvents.stream()
                .filter(event -> event.getDueDate() != null && event.getDueDate().after(new Date()) )
                .sorted(Comparator.comparing(CalendarEvent::getDueDate))
                .collect(Collectors.toList());
    }

    public Optional<Notification> getNotification(Long notificationId) {
        return Optional.ofNullable(this.notifications.stream()
                .filter(notification -> notification.hasId(notificationId))
                .findFirst().orElseThrow(() -> new NotificationNotFound()));
    }

    public List<Notification> getOldNotifications() {
        return this.notifications.stream()
                .filter(notification -> notification.getNotified().before(new Date()) )
                .sorted(Comparator.comparing(Notification::getNotified))
                .collect(Collectors.toList());
    }

    public boolean is(User other) {
        return this.getUuid().equals( other.getUuid() );
    }
}