package com.dietetika.api.model.document;

import com.dietetika.api.model.user.User;
import lombok.Data;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class MedicalStudy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String description;
    private String notes;
    private String type;
    private Date created;
    private Date updated;
    private Date deleted;
    private String uri;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false, updatable = false)
    private User user;

//    @OneToOne(optional = false, fetch = FetchType.EAGER)
//    private DocumentStorageProperties document;

    public boolean hasId(Long otherId) {
        return this.getId().equals( otherId );
    }
//
//    public String getDownloadUri() {
//        return ServletUriComponentsBuilder.fromCurrentContextPath()
//                .path("/downloadFile/")
//                .path(this.getDocument().getFileName())
//                .toUriString();
//    }
}
