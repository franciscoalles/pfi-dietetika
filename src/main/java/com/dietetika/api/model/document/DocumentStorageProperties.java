package com.dietetika.api.model.document;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "patient_document")
public class DocumentStorageProperties {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;
    private String fileName;
    private String documentType;
    private String documentFormat;
    private String uploadDir;
}