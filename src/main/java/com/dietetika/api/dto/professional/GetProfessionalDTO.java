package com.dietetika.api.dto.professional;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GetProfessionalDTO {
    private Long id;
    private String firstName;
    private String lastName;
    private String phone;
    private String email;
    private String type;
    private String description;
    private Integer yearsExperience;
    private String urlPicture;
    private String medicalId;
    private boolean premium;
}