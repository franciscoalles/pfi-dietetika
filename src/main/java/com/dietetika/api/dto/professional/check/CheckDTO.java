package com.dietetika.api.dto.professional.check;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class CheckDTO {

    private Long id;
    private Date created;
    private Date date;
    private String address;
    private String note;
    private Date updated;
}