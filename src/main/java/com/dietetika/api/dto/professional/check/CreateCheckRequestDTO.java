package com.dietetika.api.dto.professional.check;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateCheckRequestDTO {

    private String patient;
    private CheckDTO check;
}