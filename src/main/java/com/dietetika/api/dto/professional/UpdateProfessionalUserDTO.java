package com.dietetika.api.dto.professional;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class UpdateProfessionalUserDTO {
    private String uuid;
    private String email;
    private String firstName;
    private String lastName;
    private String phone;
}