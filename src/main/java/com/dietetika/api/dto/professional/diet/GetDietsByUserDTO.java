package com.dietetika.api.dto.professional.diet;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GetDietsByUserDTO {

    private Long id;
    private String name;
    private String description;
    private List<CollationDTO> collations;
    private Date updated;
}