package com.dietetika.api.dto.professional;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class CreateProfessionalUserDTO {
    private String uuid;
    private String email;
    private Date created;
    private String firstName;
    private String lastName;
}