package com.dietetika.api.dto.professional.diet;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class DeleteDietRequestDTO {
    private Long id;
    private Date deleted;
}