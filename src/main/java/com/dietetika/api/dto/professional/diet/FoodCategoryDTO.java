package com.dietetika.api.dto.professional.diet;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FoodCategoryDTO {
    private Long id;
    private String name;
    private String code;
}