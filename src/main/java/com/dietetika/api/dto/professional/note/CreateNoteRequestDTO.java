package com.dietetika.api.dto.professional.note;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class CreateNoteRequestDTO {

    private String patient;
    private NoteDTO note;
}