package com.dietetika.api.dto.professional.check;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreateCheckResponseDTO {

    private Long id;
}