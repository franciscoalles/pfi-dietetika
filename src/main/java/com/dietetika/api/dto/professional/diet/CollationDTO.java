package com.dietetika.api.dto.professional.diet;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CollationDTO {
    private String moment;
    private FoodDTO food;
    private String description;
    private Date created;
    private Date updated;
    private Date deleted;
    private Integer day;
}
