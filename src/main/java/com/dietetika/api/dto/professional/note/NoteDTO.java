package com.dietetika.api.dto.professional.note;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class NoteDTO {

    private Long id;
    private Date created;
    private String title;
    private String description;
    private Date updated;
}
