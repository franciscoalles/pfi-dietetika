package com.dietetika.api.dto.professional.diet;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
public class RegisterDietRequestDTO {
    private String name;
    private String description;
    private List<CollationDTO> collations;
    private Date created;
}