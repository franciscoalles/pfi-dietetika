package com.dietetika.api.dto.professional.note;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreateNoteResponseDTO {

    private Long id;
}