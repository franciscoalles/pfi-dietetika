package com.dietetika.api.dto.signInMobilePatient;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SignInMobilePatientRequestDTO {

    private String uuid;
}