package com.dietetika.api.dto.signInMobilePatient;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SignInMobilePatientResponseDTO {

    private String firstName;
    private String lastName;
    private String phone;
}