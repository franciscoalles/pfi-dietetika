package com.dietetika.api.dto.notification;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class GetOldNotificationsByUserDTO {

    private Long id;
    private String title;
    private String description;
    private String type;
    private Date created;
    private Date notified;
    private Date saw;
}