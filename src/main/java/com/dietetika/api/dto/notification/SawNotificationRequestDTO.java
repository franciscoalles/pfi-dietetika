package com.dietetika.api.dto.notification;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class SawNotificationRequestDTO {
    private Long id;
    private Date saw;
}