package com.dietetika.api.dto.patient.weightTracking;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class GetWeightTrackingByUserDTO {

    private Long id;
    private Float weight;
    private Date updated;
}