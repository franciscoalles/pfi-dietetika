package com.dietetika.api.dto.patient.weightTracking;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class ModifyWeightTrackRequestDTO {
    private float weight;
    private Date updated;
    private Long id;
}