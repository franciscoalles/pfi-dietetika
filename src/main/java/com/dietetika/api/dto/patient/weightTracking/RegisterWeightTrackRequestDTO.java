package com.dietetika.api.dto.patient.weightTracking;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class RegisterWeightTrackRequestDTO {
    private Float weight;
    private Date created;
}