package com.dietetika.api.dto.patient.foodTracking;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class RegisterFoodTrackRequestDTO {
    private String name;
    private Date created;
}