package com.dietetika.api.dto.patient.medicalStudies;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class DeleteMedicalStudyRequestDTO {
    private Long id;
    private Date deleted;
}