package com.dietetika.api.dto.patient.foodTracking;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class DeleteFoodTrackRequestDTO {
    private Date deleted;
    private Long id;
}