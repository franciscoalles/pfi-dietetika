package com.dietetika.api.dto.patient.medicalStudies;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GetMedicalStudyByUserDTO {

    private Long id;
    private String description;
    private String notes;
    private String type;
    private String uri;
    private Date updated;
}