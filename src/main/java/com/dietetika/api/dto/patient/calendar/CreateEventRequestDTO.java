package com.dietetika.api.dto.patient.calendar;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class CreateEventRequestDTO {

    private String title;
    private String description;
    private Date created;
    private Date dueDate;
    private Date notificationDate;
}