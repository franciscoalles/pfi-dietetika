package com.dietetika.api.dto.patient.calendar;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DeleteEventRequestDTO {

    private Long id;
}