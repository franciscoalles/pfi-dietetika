package com.dietetika.api.dto.patient;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GetPatientsFollowsDTO {
    private Long id;
    private String email;
    private Date created;
    private String firstName;
    private String lastName;
    private Date lastCheck;
    private String uuid;
}
