package com.dietetika.api.dto.patient.calendar;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class ModifyEventRequestDTO {

    private Long id;
    private String title;
    private String type;
    private String description;
    private String dueDate;
    private Date notificationDate;
}