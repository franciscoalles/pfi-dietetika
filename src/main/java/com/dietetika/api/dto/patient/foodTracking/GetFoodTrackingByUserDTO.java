package com.dietetika.api.dto.patient.foodTracking;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GetFoodTrackingByUserDTO {

    private Long id;
    private String name;
    private Date updated;
    private Date created;
    private Float kcal;
}