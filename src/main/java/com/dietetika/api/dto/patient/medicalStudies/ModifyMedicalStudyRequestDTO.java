package com.dietetika.api.dto.patient.medicalStudies;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class ModifyMedicalStudyRequestDTO {
    private Long id;
    private String description;
    private String notes;
    private String type;
    private Date updated;
}