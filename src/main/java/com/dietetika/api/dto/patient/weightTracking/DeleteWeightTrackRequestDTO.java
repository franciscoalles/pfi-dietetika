package com.dietetika.api.dto.patient.weightTracking;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class DeleteWeightTrackRequestDTO {
    private Date deleted;
    private Long id;
}