package com.dietetika.api.dto.patient;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GetPatientsNotesDTO {
    private Long id;
    private Date created;
    private String title;
    private String description;
    private Date updated;
}
