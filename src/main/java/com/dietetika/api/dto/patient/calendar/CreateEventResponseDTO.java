package com.dietetika.api.dto.patient.calendar;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
public class CreateEventResponseDTO {

    private Long id;
}