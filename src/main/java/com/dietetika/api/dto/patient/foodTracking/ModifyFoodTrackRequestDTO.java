package com.dietetika.api.dto.patient.foodTracking;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class ModifyFoodTrackRequestDTO {
    private String name;
    private Date updated;
    private Long id;
}