package com.dietetika.api.dto.patient.calendar;

import com.dietetika.api.model.calendar.CalendarEventState;
import com.dietetika.api.model.calendar.CalendarEventType;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class GetEventsByUserDTO {

    private Long id;
    private String title;
    private String description;
    private String type;
    private Date created;
    private String state;
    private Date dueDate;
    private Date notificationDate;
    private Date updated;
}