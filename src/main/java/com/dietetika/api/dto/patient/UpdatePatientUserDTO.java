package com.dietetika.api.dto.patient;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class UpdatePatientUserDTO {
    private String uuid;
    private String email;
    private String firstName;
    private String lastName;
    private String phone;
}