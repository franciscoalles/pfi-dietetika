package com.dietetika.api.dto.patient;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GetChecksWithPatientDTO {

    private Long id;
    private Date created;
    private Date date;
    private String address;
    private String note;
    private Date updated;
    private PatientDTO patient;
}
