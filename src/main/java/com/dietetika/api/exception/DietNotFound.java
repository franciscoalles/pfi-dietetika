package com.dietetika.api.exception;

public class DietNotFound extends BusinessException {
    public DietNotFound() {
        super("DIET_NOT_FOUND");
    }
}
