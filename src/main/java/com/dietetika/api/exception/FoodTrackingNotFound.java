package com.dietetika.api.exception;

public class FoodTrackingNotFound extends BusinessException {
    public FoodTrackingNotFound() {
        super("FOOD_TRACKING_NOT_FOUND");
    }
}
