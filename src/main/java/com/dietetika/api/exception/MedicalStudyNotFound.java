package com.dietetika.api.exception;

public class MedicalStudyNotFound extends BusinessException {
    public MedicalStudyNotFound() {
        super("MEDICAL_STUDY_NOT_FOUND");
    }
}
