package com.dietetika.api.exception;

public class WeightTrackingNotFound extends BusinessException {
    public WeightTrackingNotFound() {
        super("WEIGHT_TRACKING_NOT_FOUND");
    }
}
