package com.dietetika.api.exception;

public class DocumentIncorrectNameCharset extends BusinessException {
    public DocumentIncorrectNameCharset() {
        super("INVALID_FILE_NAME");
    }
}
