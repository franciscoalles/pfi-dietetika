package com.dietetika.api.exception;

public class NotificationNotFound extends BusinessException {
    public NotificationNotFound() {
        super("NOTIFICATION_NOT_FOUND");
    }
}
