package com.dietetika.api.exception;

public class ProfessionalNoFoundException extends BusinessException {

    public ProfessionalNoFoundException() {
        super("message");
    }
}