package com.dietetika.api.exception;

public class PatientNoFoundException extends BusinessException {

    public PatientNoFoundException() {
        super("message");
    }
}