package com.dietetika.api.exception;

public class DocumentCreationFailed extends BusinessException {
    public DocumentCreationFailed() {
        super("FAIL_CREATION_FILE");
    }
}
