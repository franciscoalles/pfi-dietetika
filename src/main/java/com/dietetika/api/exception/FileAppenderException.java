package com.dietetika.api.exception;

import java.io.IOException;

public class FileAppenderException extends Throwable {
    private static final long serialVersionUID = -2823496175268225590L;

    public FileAppenderException(IOException e, String filename) {
        super("No es posible crear el archivo. Directorio del archivo: " + filename, e);
    }
}
