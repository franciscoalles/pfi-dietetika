package com.dietetika.api.exception;

public class CalendarEventNotFound extends BusinessException {
    public CalendarEventNotFound() {
        super("EVENT_NOT_FOUND");
    }
}
