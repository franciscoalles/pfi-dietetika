package com.dietetika.api.repository;

import com.dietetika.api.model.user.Patient;
import com.dietetika.api.model.user.Professional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PatientRepository extends CrudRepository<Patient, Long> {

    Optional<Patient> findByUuid(String uuid);

    Optional<Patient> findByUuidAndDeletedIsNull(String uuid);

    List<Patient> findByDeletedIsNull();
}