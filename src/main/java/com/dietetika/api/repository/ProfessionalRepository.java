package com.dietetika.api.repository;

import com.dietetika.api.model.user.Professional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProfessionalRepository extends CrudRepository<Professional, Long> {

    Optional<Professional> findByUuid(String uuid);

    Optional<Professional> findByUuidAndDeletedIsNull(String uuid);

    List<Professional> findByDeletedIsNull();
}