package com.dietetika.api.repository;

import com.dietetika.api.model.calendar.CalendarEvent;
import com.dietetika.api.model.notification.Notification;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationRepository extends CrudRepository<Notification, Long> {

}