package com.dietetika.api.repository;

import com.dietetika.api.model.diet.Diet;
import com.dietetika.api.model.user.Patient;
import com.dietetika.api.model.user.Professional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DietRepository extends CrudRepository<Diet, Long> {

    List<Diet> findAllByProfessional_UuidOrProfessionalIsNull(String uuid);
}