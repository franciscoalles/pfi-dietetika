package com.dietetika.api.repository;

import com.dietetika.api.model.document.DocumentStorageProperties;
import com.dietetika.api.model.document.MedicalStudy;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicalStudiesRepository extends CrudRepository<MedicalStudy, Long> {

}