package com.dietetika.api.repository;

import com.dietetika.api.model.tracking.FoodTracking;
import com.dietetika.api.model.tracking.WeightTracking;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WeightTrackingRepository extends CrudRepository<WeightTracking, Long> {

}