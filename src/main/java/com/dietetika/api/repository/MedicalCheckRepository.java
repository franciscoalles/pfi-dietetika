package com.dietetika.api.repository;

import com.dietetika.api.model.check.MedicalCheck;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface MedicalCheckRepository extends CrudRepository<MedicalCheck, Long> {

    List<MedicalCheck> findAllByProfessional_Uuid(String uuid);
}