package com.dietetika.api.repository;

import com.dietetika.api.model.calendar.CalendarEvent;
import com.dietetika.api.model.tracking.FoodTracking;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FoodTrackingRepository extends CrudRepository<FoodTracking, Long> {

}