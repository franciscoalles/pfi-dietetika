# PFI-Dietetika (Backend)
Bienvenido al proyecto de Dietetika!
Este módulo representa a la servicio de Backend, es decir, las APIs que ofrecen asistencia, lógica y persistencia a las Apps.
---
REQUISITOS
-
Para lavantar el proyecto se debe cumplir con los siguientes requisitos:

- JDK 1.11 como mínimo
- Apache Maven 3.3 cómo mínimo
- IDE (eclipse, NetBeans, IntelliJ Idea, etc)
---
Ejecución del proyecto
-
1. Para poder ejecutar el proyecto, se lo debe imporat como proyecto Maven, seleccionando el `pom.xml`.
2. Una vez que el proyecto se encuentre cargado, asegurarse que todas las dependencias de Maven se encuentren descargadas, de lo contrario, el proyecto no se ejecutara o bien, indicará errores.
3. Ubicar la clase `ApiApplication` y ejecutarla, la misma comenzará a levantar el servicio y mostrará algo como esto:

````.   ____          _            __ _ _
/\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
\\/  ___)| |_)| | | | | || (_| |  ) ) ) )
'  |____| .__|_| |_|_| |_\__, | / / / /
=========|_|==============|___/=/_/_/_/
:: Spring Boot ::                (v2.5.3)

2021-10-31 15:48:19.955  INFO 17124 --- [  restartedMain] com.dietetika.api.ApiApplication         : Starting ApiApplication using Java 11.0.12 on Fedde-Lenovo with PID 17124 (C:\Users\Fede\Repositorios\dietetika-backend\target\classes started by Fede in C:\Users\Fede\Repositorios\dietetika-backend)
2021-10-31 15:48:19.957  INFO 17124 --- [  restartedMain] com.dietetika.api.ApiApplication         : No active profile set, falling back to default profiles: default
2021-10-31 15:48:20.004  INFO 17124 --- [  restartedMain] .e.DevToolsPropertyDefaultsPostProcessor : Devtools property defaults active! Set 'spring.devtools.add-properties' to 'false' to disable
2021-10-31 15:48:20.004  INFO 17124 --- [  restartedMain] .e.DevToolsPropertyDefaultsPostProcessor : For additional web related logging consider setting the 'logging.level.web' property to 'DEBUG'
2021-10-31 15:48:20.980  INFO 17124 --- [  restartedMain] .s.d.r.c.RepositoryConfigurationDelegate : Bootstrapping Spring Data JPA repositories in DEFAULT mode.
2021-10-31 15:48:21.056  INFO 17124 --- [  restartedMain] .s.d.r.c.RepositoryConfigurationDelegate : Finished Spring Data repository scanning in 70 ms. Found 8 JPA repository interfaces.
2021-10-31 15:48:21.527  INFO 17124 --- [  restartedMain] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8081 (http)
2021-10-31 15:48:21.535  INFO 17124 --- [  restartedMain] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2021-10-31 15:48:21.535  INFO 17124 --- [  restartedMain] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.50]
2021-10-31 15:48:21.601  INFO 17124 --- [  restartedMain] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2021-10-31 15:48:21.601  INFO 17124 --- [  restartedMain] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 1596 ms
2021-10-31 15:48:21.782  INFO 17124 --- [  restartedMain] o.hibernate.jpa.internal.util.LogHelper  : HHH000204: Processing PersistenceUnitInfo [name: default]
2021-10-31 15:48:21.830  INFO 17124 --- [  restartedMain] org.hibernate.Version                    : HHH000412: Hibernate ORM core version 5.4.32.Final
2021-10-31 15:48:21.947  INFO 17124 --- [  restartedMain] o.hibernate.annotations.common.Version   : HCANN000001: Hibernate Commons Annotations {5.1.2.Final}
2021-10-31 15:48:22.034  INFO 17124 --- [  restartedMain] com.zaxxer.hikari.HikariDataSource       : HikariPool-1 - Starting...
2021-10-31 15:48:22.338  INFO 17124 --- [  restartedMain] com.zaxxer.hikari.HikariDataSource       : HikariPool-1 - Start completed.
2021-10-31 15:48:22.353  INFO 17124 --- [  restartedMain] org.hibernate.dialect.Dialect            : HHH000400: Using dialect: org.hibernate.dialect.MySQL8Dialect
2021-10-31 15:48:22.525  WARN 17124 --- [  restartedMain] org.hibernate.cfg.AnnotationBinder       : HHH000457: Joined inheritance hierarchy [com.dietetika.api.model.user.User] defined explicit @DiscriminatorColumn.  Legacy Hibernate behavior was to ignore the @DiscriminatorColumn.  However, as part of issue HHH-6911 we now apply the explicit @DiscriminatorColumn.  If you would prefer the legacy behavior, enable the `hibernate.discriminator.ignore_explicit_for_joined` setting (hibernate.discriminator.ignore_explicit_for_joined=true)
2021-10-31 15:48:24.172  INFO 17124 --- [  restartedMain] o.h.e.t.j.p.i.JtaPlatformInitiator       : HHH000490: Using JtaPlatform implementation: [org.hibernate.engine.transaction.jta.platform.internal.NoJtaPlatform]
2021-10-31 15:48:24.179  INFO 17124 --- [  restartedMain] j.LocalContainerEntityManagerFactoryBean : Initialized JPA EntityManagerFactory for persistence unit 'default'
2021-10-31 15:48:24.623  WARN 17124 --- [  restartedMain] JpaBaseConfiguration$JpaWebConfiguration : spring.jpa.open-in-view is enabled by default. Therefore, database queries may be performed during view rendering. Explicitly configure spring.jpa.open-in-view to disable this warning
2021-10-31 15:48:24.906  INFO 17124 --- [  restartedMain] o.s.b.d.a.OptionalLiveReloadServer       : LiveReload server is running on port 35729
2021-10-31 15:48:24.934  INFO 17124 --- [  restartedMain] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8081 (http) with context path ''
2021-10-31 15:48:24.944  INFO 17124 --- [  restartedMain] com.dietetika.api.ApiApplication         : Started ApiApplication in 5.418 seconds (JVM running for 6.704)
2021-10-31 15:48:32.553  INFO 17124 --- [ionShutdownHook] j.LocalContainerEntityManagerFactoryBean : Closing JPA EntityManagerFactory for persistence unit 'default'
2021-10-31 15:48:32.557  INFO 17124 --- [ionShutdownHook] com.zaxxer.hikari.HikariDataSource       : HikariPool-1 - Shutdown initiated...
2021-10-31 15:48:32.570  INFO 17124 --- [ionShutdownHook] com.zaxxer.hikari.HikariDataSource       : HikariPool-1 - Shutdown completed.````
